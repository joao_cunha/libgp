#!/bin/bash

if [ $# -ne 5 ]; then
	echo "invalid number of parameters"
	echo "usage: $0 <#trials> <min_size> <inc> <train_size> <test_size>"
	exit 1
fi

NUM_TRIALS=$1
MIN_SIZE=$2
INC=$3
TRAIN_SIZE=$4
TEST_SIZE=$5

EXEC_DIR=/home/alex/Robotics/libgp/build/examples
EXEC_CMD=gpcomparison

RM="rm -rf"

DATA="*.txt *.gp *.cluster"

for NOISE_COEF in $(seq 0.0 0.1 6.0)
do

	echo "noise_coef $NOISE_COEF"
	DEST_DIR=/home/alex/Robotics/libgp/data/${TRAIN_SIZE}_${TEST_SIZE}_${NOISE_COEF}

	echo "making dir: $DEST_DIR"
	mkdir -p $DEST_DIR

	for CLUSTER_SIZE in $(seq $MIN_SIZE $INC $TRAIN_SIZE)
	do

		echo "Size $TRAINSIZE" 
		SUB_DEST_DIR=${DEST_DIR}/${TRAIN_SIZE}_${TEST_SIZE}_${CLUSTER_SIZE}

		echo "making dir: $SUB_DEST_DIR"
		mkdir -p $SUB_DEST_DIR
		
		for i in $(seq 1 $NUM_TRIALS)
		do

			echo "Trial $i" 

			echo "making temp"
			mkdir -p ${SUB_DEST_DIR}/temp

			cd ${SUB_DEST_DIR}/temp
		
			echo "${EXEC_DIR}/${EXEC_CMD} ${TRAIN_SIZE} ${TEST_SIZE} ${CLUSTER_SIZE} ${NOISE_COEF}"
			${EXEC_DIR}/${EXEC_CMD} ${TRAIN_SIZE} ${TEST_SIZE} ${CLUSTER_SIZE} ${NOISE_COEF} > $i.txt

			#archive data
		#	echo "zip -b $DEST_DIR $i $DATA statistics_$i.txt"
			zip $i $DATA > /dev/null

			#move archive to dest_dir
			mv $i.zip $SUB_DEST_DIR

			#move $i.txt to dest_dir
			mv $i.txt $SUB_DEST_DIR
			
			cd $SUB_DEST_DIR

			$RM temp
		done
	done
done
