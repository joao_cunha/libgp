#!/bin/bash

if [ $# -ne 9 ]; then
	echo "invalid number of parameters"
	echo "usage: $0 <#dof> <#trials> <min_size> <inc_size> <max_size> <noise_hyper> <min_noise> <inc_noise> <max_noise>"
	exit 1
fi


DOF=$1
NUM_TRIALS=$2
PREFIX=real
MINSIZE=$3
INCSIZE=$4
MAXSIZE=$5
NOISE=$6
MINNOISE=$7
INCNOISE=$8
MAXNOISE=$9

DATA_DIR=/home/alex/Robotics/Barrett
EXEC_DIR=/home/alex/Robotics/libgp/build/examples
EXEC_CMD=gpdofmodel
RM="rm -rf"
DATA="*.txt *.gp *.cluster"



for NOISE_COEF in $(seq $MINNOISE $INCNOISE $MAXNOISE)
do

	echo "noise_coef $NOISE_COEF"
	DEST_DIR=/home/alex/Robotics/libgp/data/${PREFIX}${DOF}_${NOISE_COEF}

	echo "making dir: $DEST_DIR"
	mkdir -p $DEST_DIR

	for TRAINSIZE in $(seq $MINSIZE $INCSIZE $MAXSIZE)
	do

		echo "Size $TRAINSIZE" 
		SUB_DEST_DIR=${DEST_DIR}/12000_3000_$TRAINSIZE

		echo "making dir: $SUB_DEST_DIR"
		mkdir -p $SUB_DEST_DIR

		
		for i in $(seq 1 $NUM_TRIALS)
		do

			echo "Trial $i" 

			echo "making temp"
			mkdir -p ${SUB_DEST_DIR}/temp
			
			cd ${SUB_DEST_DIR}/temp		

			echo "${EXEC_DIR}/${EXEC_CMD} $DATA_DIR/$PREFIX $DOF $TRAINSIZE $NOISE $NOISE_COEF > $i.txt"
			${EXEC_DIR}/${EXEC_CMD} ${DATA_DIR}/${PREFIX} $DOF $TRAINSIZE $NOISE $NOISE_COEF > $i.txt

			#archive data
		#	echo "zip -b $DEST_DIR $i $DATA statistics_$i.txt"
			zip $i $DATA > /dev/null

			#move archive to dest_dir
			cp $i.zip $SUB_DEST_DIR

			#move $i.txt to dest_dir
			cp $i.txt $SUB_DEST_DIR

			cd ${SUB_DEST_DIR}

			#cleanup
			$RM temp
		done
	done
done
