/*
 * gp_example_dof_model.cc
 *
 *  Created on: Apr 8, 2014
 *      Author: alex
 */


#define GP_FULL 0
#define OPTIMIZE 0
#define HYPER 0

#include "gp_full.h"
#include "gp_local.h"
#include "gp_utils.h"
#include "rprop.h"
#include "cg.h"

#include <string>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

using namespace libgp;
using namespace std;



void loadDataset(string filename, unsigned int inputDim, vector<double*>& input, vector<double>& output)
{
	ifstream ifs(filename.c_str(), ifstream::in);

	string line;

	while(getline(ifs, line))
	{
		stringstream ss;
		ss.str(line);
		double* x = new double[inputDim];
		for (unsigned int i = 0; i < inputDim; ++i)
		{
			ss>> x[i];
//			cout << x[i] << " ";
		}

		input.push_back(x);

		double y;

		ss>> y;

//		cout << "| " << y << endl;

		output.push_back(y);


	}

	ifs.close();
}

void addDataset(GaussianProcess& gp, vector<double*> input, vector<double> output)
{
	for (unsigned int i = 0; i < input.size(); ++i)
	{
		double* x = input[i];
		double y = output[i];

		gp.add_pattern(x,y);
	}
}

void optimize(GaussianProcess& gp, unsigned int epochs, bool verbose)
{
	RProp opt;

	gp.optimize(&opt, epochs, verbose);
}


double testDataset(GaussianProcess& gp, unsigned int inputDim, vector<double*> input, vector<double> output, string filename)
{
	double tss = 0.0;
	vector <double> meanPred, varPred;

	for(unsigned int i = 0; i < input.size(); ++i)
	{
		double* x = input[i];
		double y = output[i];

		double f = gp.f(x);
		double error = f - y;
		tss += error*error;

		meanPred.push_back(f);
		varPred.push_back(gp.var(x));
	}

	std::ofstream predStream(filename.c_str(), std::ofstream::trunc);

	for (unsigned int i = 0; i < input.size(); ++i)
	{

		double *x = input[i];
		for (unsigned int j = 0; j < inputDim; ++j)
		{
			predStream << x[j] << " ";
		}

		predStream << output[i] << " " << meanPred[i] << " " << varPred[i] << endl;

	}

	predStream.close();

	return tss;
}



int main (int argc, char const *argv[])
{

	if(argc != 6)
	{
		cout << "Usage: " << argv[0] << " <prefix> <dof_#> <maxSize> <noise> <local_gp_noise_coef>" << endl;

	}

	//process arguments
	string prefix = argv[1];
	unsigned int dof = atoi(argv[2]);
	unsigned int maxSize = atoi(argv[3]);
	bool noise_hyper = atoi(argv[4]) != 0;
	bool do_local_gp = atof(argv[5]) >= 0.0;
	double noise_coef = atof(argv[5]);

	cout << "prefix " << prefix << ", dof = " << dof << ", maxSize = " << maxSize << ", hyper_noise = " << noise_hyper << ", noise_coef = << " << noise_coef << endl;

	struct timeval tv1, tv2;
	double dur;

	unsigned int inputDim = 21;

#if GP_FULL
	FullGaussianProcess full_gp(inputDim, "CovSum ( CovSEard, CovNoise)");
#endif

	LocalGaussianProcess local_gp(inputDim, "CovSum ( CovSEard, CovNoise)", maxSize, None, noise_coef);
	LocalGaussianProcess noisy_local_gp(inputDim, "CovSum ( CovSEard, CovNoise)", maxSize, Noise, noise_coef);
	LocalGaussianProcess random_local_gp(inputDim, "CovSum ( CovSEard, CovNoise)", maxSize, Random, noise_coef);


	Eigen::VectorXd params(inputDim+2); //+1 sigma_f, +1 sigma_n

	params.setZero();
	params(inputDim+2-1) = log(0.1);

	if(noise_hyper)
	{
		struct timeval tv;
		gettimeofday( &tv , NULL );
		long unsigned int seed = tv.tv_sec*1000+tv.tv_usec/1000;

		srand(seed);

		Eigen::VectorXd noise(inputDim+2);

		params = params + noise.setRandom()*5;
	}


	switch(dof)
	{
		case 1:
			params << 0.511291, 2.76593, 381.155, 395.604, 409.892, 395.4, 379.49, -3.35779, 2.87369, -0.165035, 409.354, 381.155, 391.797, 409.984, 1.02303, 4.6641, 2.38747, 381.359, 396.154, 381.529, 6.58424, 1.01874, -0.0425774;
			break;
		case 2:
			params << 5.75155, 0.143986, 3.47963, 1.17407, 390.067, 5.02853, 4.9074, 409.686, -1.76898, -0.131638, 4.87984, 409.686, 409.686, 6.47826, 5.78405, 2.26659, 6.84597, 4.10889, 409.686, 7.07431, 7.81867, 2.43641, -1.0179;
			break;
		case 3:
			params << 380.588, -0.150289, -0.469721, 1.12903, 403.975, 2.52095, 403.257, 0.0744366, -1.73089, -1.51862, 394.923, 380.353, 394.953, 403.975, 1.9583, 2.03896, 2.10922, 390.386, 393.623, 5.6127, 385.012, 0.108014, -1.54023;
			break;
		case 4:
			params << 4.58612, 0.47257, 3.85266, -0.23331, 404.956, 4.03033, 4.6665, 380.353, 4.68761, 398.484, -1.96415, 6.54624, 390.846, 5.40139, 380.607, 3.19027, 380.302, 3.28956, 7.52786, 6.80962, 375.504, 1.23793, -1.70451;
			break;
		case 5:
			params << 404.279, 1.87648, 1.93682, 5.28858, 0.999556, 1.86959, 2.52966, 1.02762, 2.96777, 404.745, 404.533, -1.31631, -1.03473, 2.92101, 3.02857, 4.67243, 409.686, 404.745, 3.77274, 383.693, 4.62044, -1.56901, -2.42429;
			break;
		case 6:
			params << 1.20839, 384.412, 1.98711, 1.37809, 0.874314, 2.19872, 404.181, 1.27703, 404.533, 404.533, 404.533, -1.11125, -0.93993, 404.533, 3.55628, 3.11211, 404.745, 3.68492, 409.686, 3.21257, 3.16486, -1.568, -2.36484;
			break;
		case 7:
			params << -1.49017,-0.506598, 4.12227, -2.05573, -2.28361, -0.355522, -4.61574, -1.04613, -1.13531, 4.0898, 0.913575, 2.56586, 4.47488, 3.42223, -4.32231, 4.2381, 4.8689, 2.72921, 2.80279, -1.30209, 3.36874, 1.79274, -1.79745;
			break;
		default:
			return EXIT_FAILURE;
	}

	cout << "GPML params = " << params.transpose() << endl;
	//Initialize parameters

#if GP_FULL
	full_gp.covf().set_loghyper(params);
#endif

	if(do_local_gp)
	{
		local_gp.covf().set_loghyper(params);
		noisy_local_gp.covf().set_loghyper(params);
		random_local_gp.covf().set_loghyper(params);
	}


	//optimize hyper parameters
#if HYPER

	std::vector<double*> inputHyper;
	std::vector<double> outputHyper;

	//load hyper dataset
	std::stringstream ssHyper;

	ssHyper << prefix << "Dof" << dof << "_hyper.txt";

	cout << ssHyper.str() << endl;

	loadDataset(ssHyper.str(), inputDim, inputHyper, outputHyper);

	cout << "Hyper samples = " << inputHyper.size() << endl;

	FullGaussianProcess hyper_gp(inputDim, "CovSum ( CovSEard, CovNoise)");

	Eigen::VectorXd log_hyper(inputDim+2);

	log_hyper = params;

	hyper_gp.covf().set_loghyper(log_hyper);

	addDataset(hyper_gp, inputHyper, outputHyper);

	unsigned int epochs = 100;
	optimize(hyper_gp, epochs, true);

	//set optimized hyper parameters to other gp's
	log_hyper = hyper_gp.covf().get_loghyper();

	cout << "Optimized params = " << log_hyper.transpose() << endl;

	//set optimized hyper params
#if GP_FULL
	full_gp.covf().set_loghyper(log_hyper);
#endif

	if(do_local_gp)
	{
//		local_gp.covf().set_loghyper(log_hyper);
		noisy_local_gp.covf().set_loghyper(log_hyper);
//		random_local_gp.covf().set_loghyper(log_hyper);
	}

//	testDataset(hyper_gp, inputDim, inputHyper, outputHyper, "hyper_pred.txt");

	hyper_gp.write("hyper.gp");

//	return EXIT_SUCCESS;



#endif

	std::vector<double*> inputTrain;
	std::vector<double> outputTrain;

	//load train dataset
	std::stringstream ssTrain;

	ssTrain << prefix << "Dof" << dof << "_train.txt";

	cout << ssTrain.str() << endl;

	loadDataset(ssTrain.str(), inputDim, inputTrain, outputTrain);

	cout << "Training samples = " << inputTrain.size() << endl;

	//add pattern to GPs
#if GP_FULL
	gettimeofday( &tv1 , NULL );
	addDataset(full_gp, inputTrain, outputTrain);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Full GP add patterns = " << dur << std::endl;

//	testDataset(full_gp, inputDim, inputTrain, outputTrain, "pred_train.txt");
#endif


	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
//		addDataset(local_gp, inputTrain, outputTrain);
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Local GP add patterns = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
		addDataset(noisy_local_gp, inputTrain, outputTrain);
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Noisy GP add patterns = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
//		addDataset(random_local_gp, inputTrain, outputTrain);
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Random GP add patterns = " << dur << std::endl;



#if OPTIMIZE

	bool verbose = false;
	RProp rprop;

#	if GP_FULL
	gettimeofday( &tv1 , NULL );
	full_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Full GP optimization = " << dur << std::endl;
#	endif

	gettimeofday( &tv1 , NULL );
	local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	noisy_local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	random_local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;
#endif

	//load test dataset
	std::vector<double*> inputTest;
	std::vector<double> outputTest;

	stringstream ssTest;

	ssTest << prefix << "Dof" << dof << "_test.txt";

	cout << ssTest.str() << endl;

	cout << prefix << "Dof" << dof << "_test.txt" << endl;

	loadDataset(ssTest.str(), inputDim, inputTest, outputTest);

	cout << "Testing samples = " << inputTest.size() << endl;

	double full_tss = 0, local_tss = 0, noisy_tss = 0, random_tss = 0;

	gettimeofday( &tv1 , NULL );
#if GP_FULL
	full_tss = testDataset(full_gp, inputDim, inputTest, outputTest, "full_pred.txt");
#endif
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Full GP inference = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
//		local_tss = testDataset(local_gp, inputDim, inputTest, outputTest, "local_pred.txt");
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP inference = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
		noisy_tss = testDataset(noisy_local_gp, inputDim, inputTest, outputTest, "noisy_local_pred.txt");
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Noisy GP inference = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	if(do_local_gp)
	{
//		random_tss = testDataset(random_local_gp, inputDim, inputTest, outputTest, "random_local_pred.txt");
	}
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Noisy GP inference = " << dur << std::endl;

	std::cout << "full mse = " << full_tss/inputTest.size() << std::endl;
	std::cout << "local mse = " << local_tss/inputTest.size() << std::endl;
	std::cout << "noisy mse = " << noisy_tss/inputTest.size() << std::endl;
	std::cout << "random mse = " << random_tss/inputTest.size() << std::endl;

#if GP_FULL
	full_gp.write("full.gp");
#endif

	if(do_local_gp)
	{
//		for (unsigned int i = 0; i< local_gp.get_num_clusters(); ++i)
//		{
//
//			std::stringstream ss;
//			ss << "local" << i << ".cluster";
//			std::ofstream localOfs(ss.str().c_str(), std::ofstream::trunc);
//
//			localOfs << local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;
//
//			FullGaussianProcess* gp = local_gp.get_cluster(i).gp;
//			for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
//			{
//				localOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
//			}
//			localOfs.close();
//
//			ss.str("");
//			ss << "local" << i << ".gp";
//
//			gp->write(ss.str().c_str());
//		}


		for (unsigned int i = 0; i< noisy_local_gp.get_num_clusters(); ++i)
		{

			std::stringstream ss;
			ss << "noisy" << i << ".cluster";
			std::ofstream noisyOfs(ss.str().c_str(), std::ofstream::trunc);

			noisyOfs << noisy_local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;

			FullGaussianProcess* gp = noisy_local_gp.get_cluster(i).gp;
			for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
			{
				noisyOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
			}
			noisyOfs.close();

			ss.str("");
			ss << "noisy" << i << ".gp";

			gp->write(ss.str().c_str());
		}

//		for (unsigned int i = 0; i< random_local_gp.get_num_clusters(); ++i)
//		{
//
//			std::stringstream ss;
//			ss << "random" << i << ".cluster";
//			std::ofstream randomOfs(ss.str().c_str(), std::ofstream::trunc);
//
//			randomOfs << random_local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;
//
//			FullGaussianProcess* gp = random_local_gp.get_cluster(i).gp;
//			for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
//			{
//				randomOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
//			}
//			randomOfs.close();
//
//			ss.str("");
//			ss << "random" << i << ".gp";
//
//			gp->write(ss.str().c_str());
//		}

	}

	return EXIT_SUCCESS;

}



