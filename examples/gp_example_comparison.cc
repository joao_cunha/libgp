/*
 * gp_example_comparison.cc
 *
 *  Created on: Oct 21, 2013
 *      Author: alex
 */

#define GP_FULL 1
#define OPTIMIZE 1


#include "gp_full.h"
#include "gp_local.h"
#include "gp_utils.h"
#include "rprop.h"

#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <string>
#include <sstream>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

using namespace libgp;

int main (int argc, char const *argv[])
{
	struct timeval tv1, tv2;
	double dur;


	double full_tss = 0, local_tss = 0, noisy_tss = 0, random_tss = 0;

	// initialize Gaussian process for 2-D input using the squared exponential
	// covariance function with additive white noise.

#if GP_FULL
	FullGaussianProcess full_gp(2, "CovSum ( CovSEard, CovNoise)");
#endif




	if(argc != 5)
	{
		std::cout << "usage: " << argv[0] << " <train_size> <test_size> <cluster_size> <noise_coef>" << std::endl;
		exit(1);
	}

	unsigned int n=atoi(argv[1]), m=atoi(argv[2]);
	unsigned int cluster_size = atoi(argv[3]);
	double noise_coef = atof(argv[4]);


	LocalGaussianProcess local_gp(2, "CovSum ( CovSEard, CovNoise)", cluster_size, None, noise_coef);
	LocalGaussianProcess noisy_local_gp(2, "CovSum ( CovSEard, CovNoise)", cluster_size, Noise, noise_coef);
	LocalGaussianProcess random_local_gp(2, "CovSum ( CovSEard, CovNoise)", cluster_size, Random, noise_coef);

	std::cout << "Train samples = " << n << std::endl;
	std::cout << "Test samples = " << m << std::endl;
	std::cout << "Max cluster size = " << cluster_size << std::endl;

	// initialize hyper parameter vector
//	Eigen::VectorXd params(full_gp.covf().get_param_dim());
	Eigen::VectorXd params(4);
	params << 0.0, 0.0, 0.0, -2.0;

	// set parameters of covariance function

#if GP_FULL
	full_gp.covf().set_loghyper(params);
#endif
	local_gp.covf().set_loghyper(params);
	noisy_local_gp.covf().set_loghyper(params);
	random_local_gp.covf().set_loghyper(params);

	std::ofstream trainDataStream("trainData.txt", std::ofstream::trunc);

	std::vector<double*> inputTrain;
	std::vector<double> outputTrain;

	// add training patterns
	for(unsigned int i = 0; i < n; ++i)
	{

		double* x = new double[2];
		x[0] = drand48()*4-2;
		x[1] = drand48()*4-2;
		double y = Utils::hill(x[0], x[1]) + Utils::randn() * 0.3;

		inputTrain.push_back(x);
		outputTrain.push_back(y);

		//write to file
		for (unsigned int j = 0; j < 2; ++j)
		{
			trainDataStream << x[j] << " ";
		}
		trainDataStream << y << std::endl;
	}
	trainDataStream.close();

#if GP_FULL
	gettimeofday( &tv1 , NULL );

	for (unsigned int i = 0; i < n; ++i)
	{

		double* x = inputTrain[i];
		double y = outputTrain[i];

		full_gp.add_pattern(x,y);

	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Full GP add patterns = " << dur << std::endl;
#endif

	gettimeofday( &tv1 , NULL );

//	for (int i = 0; i < n; ++i)
//	{
//
//		double* x = inputTrain[i];
//		double y = outputTrain[i];
//
//		local_gp.add_pattern(x, y);
//	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Local GP add patterns = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );

	for (unsigned int i = 0; i < n; ++i)
	{

		double* x = inputTrain[i];
		double y = outputTrain[i];

		noisy_local_gp.add_pattern(x,y);
	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Noisy GP add patterns = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );

//	for (int i = 0; i < n; ++i)
//	{
//
//		double* x = inputTrain[i];
//		double y = outputTrain[i];
//
//		random_local_gp.add_pattern(x,y);
//	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Random GP add patterns = " << dur << std::endl;

#if OPTIMIZE

	bool verbose = false;
	RProp rprop;
//	rprop.init(0.001);

#	if GP_FULL
	gettimeofday( &tv1 , NULL );
	full_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Full GP optimization = " << dur << std::endl;
#	endif

	gettimeofday( &tv1 , NULL );
//	local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
	noisy_local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );
//	random_local_gp.optimize(&rprop, 100, verbose);
	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);
	std::cout << "Time Local GP optimization = " << dur << std::endl;
#endif

	std::ofstream testDataStream("testData.txt", std::ofstream::trunc);

	std::vector<double*> inputTest;
	std::vector<double> outputTest;

	// total squared error
	for(unsigned int i = 0; i < m; ++i)
	{
//		std::cout << "Test sample " << i << std::endl;

		double* x = new double[2];
		x[0] = drand48()*4-2;
		x[1] = drand48()*4-2;
		double y = Utils::hill(x[0], x[1]);

		inputTest.push_back(x);
		outputTest.push_back(y);

		//write to file
		for (unsigned int j = 0; j < 2; ++j)
		{
			testDataStream << x[j] << " ";
		}
		testDataStream << y << std::endl;
	}
	testDataStream.close();

	std::vector<double> meanPred;
	std::vector<double> varPred;


	gettimeofday( &tv1 , NULL );

	for(unsigned int i = 0; i < m; ++i)
	{
#if GP_FULL
		double* x = inputTest[i];
		double y = outputTest[i];

		double f = full_gp.f(x);
		double error = f - y;
		full_tss += error*error;

		meanPred.push_back(f);
		varPred.push_back(full_gp.var(x));
#else
		meanPred.push_back(0);
		varPred.push_back(0);
#endif

	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Full GP inference = " << dur << std::endl;


	gettimeofday( &tv1 , NULL );

//	for(int i = 0; i < m; ++i)
//	{
//		double* x = inputTest[i];
//		double y = outputTest[i];
//
//		double f = local_gp.f(x);
//		double error = f - y;
//		local_tss += error*error;
//
//		meanPred.push_back(f);
//		varPred.push_back(local_gp.var(x));
//	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Local GP inference = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );

	for(unsigned int i = 0; i < m; ++i)
	{
		double* x = inputTest[i];
		double y = outputTest[i];

		double f = noisy_local_gp.f(x);
		double error = f - y;
		noisy_tss += error*error;

		meanPred.push_back(f);
		varPred.push_back(noisy_local_gp.var(x));

	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Noisy GP inference = " << dur << std::endl;

	gettimeofday( &tv1 , NULL );

//	for(int i = 0; i < m; ++i)
//	{
//		double* x = inputTest[i];
//		double y = outputTest[i];
//
//		double f = random_local_gp.f(x);
//		double error = f - y;
//		random_tss += error*error;
//
//		meanPred.push_back(f);
//		varPred.push_back(random_local_gp.var(x));
//	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time Noisy GP inference = " << dur << std::endl;

	std::ofstream predStream("predictions.txt", std::ofstream::trunc);

	for (unsigned int i = 0; i < m; ++i)
	{
		predStream << outputTest[i] << " " <<
				meanPred[i] << " " << varPred[i] << " " << //full_gp
				meanPred[m+i] << " " << varPred[m+i] << " " << //local_gp
				meanPred[2*m+i] << " " << varPred[2*m+i] << " " << //noisy_gp
				meanPred[3*m+i] << " " << varPred[3*m+i] << std::endl;
	}



	predStream.close();

	std::cout << "full mse = " << full_tss/m << std::endl;
	std::cout << "local mse = " << local_tss/m << std::endl;
	std::cout << "noisy mse = " << noisy_tss/m << std::endl;
	std::cout << "random mse = " << random_tss/m << std::endl;

#if GP_FULL
	full_gp.write("full.gp");
#endif


//	for (unsigned int i = 0; i< local_gp.get_num_clusters(); ++i)
//	{
//
//		std::stringstream ss;
//		ss << "local" << i << ".cluster";
//		std::ofstream localOfs(ss.str().c_str(), std::ofstream::trunc);
//
//		localOfs << local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;
//
//		FullGaussianProcess* gp = local_gp.get_cluster(i).gp;
//		for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
//		{
//			localOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
//		}
//		localOfs.close();
//
//		ss.str("");
//		ss << "local" << i << ".gp";
//
//		gp->write(ss.str().c_str());
//	}


	for (unsigned int i = 0; i< noisy_local_gp.get_num_clusters(); ++i)
	{

		std::stringstream ss;
		ss << "noisy" << i << ".cluster";
		std::ofstream noisyOfs(ss.str().c_str(), std::ofstream::trunc);

		noisyOfs << noisy_local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;

		FullGaussianProcess* gp = noisy_local_gp.get_cluster(i).gp;
		for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
		{
			noisyOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
		}
		noisyOfs.close();

		ss.str("");
		ss << "noisy" << i << ".gp";

		gp->write(ss.str().c_str());
	}

//	for (unsigned int i = 0; i< random_local_gp.get_num_clusters(); ++i)
//	{
//
//		std::stringstream ss;
//		ss << "random" << i << ".cluster";
//		std::ofstream randomOfs(ss.str().c_str(), std::ofstream::trunc);
//
//		randomOfs << random_local_gp.get_cluster(i).centroid.transpose() << " " << 0 << std::endl;
//
//		FullGaussianProcess* gp = random_local_gp.get_cluster(i).gp;
//		for (unsigned int j = 0; j < gp->get_sampleset_size(); ++j)
//		{
//			randomOfs << gp->sampleset->x(j).transpose() << " " << gp->sampleset->y(j) << std::endl;
//		}
//		randomOfs.close();
//
//		ss.str("");
//		ss << "random" << i << ".gp";
//
//		gp->write(ss.str().c_str());
//	}


	return EXIT_SUCCESS;

}
