/*
 * cg.h
 *
 *  Created on: Feb 22, 2013
 *      Author: Joao Cunha <joao.cunha@ua.pt>
 */

#ifndef CG_H_
#define CG_H_

#include "optimizer.h"
#include "gp_full.h"

namespace libgp
{

class CG : public Optimizer
{
public:
	CG();
	virtual ~CG();
	virtual void maximize(FullGaussianProcess* gp, size_t n, bool verbose);
};

}

#endif /* CG_H_ */
