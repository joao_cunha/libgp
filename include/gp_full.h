/*
 * gp_full.h
 *
 *  Created on: Sep 27, 2013
 *      Author: alex
 */

#ifndef GP_FULL_H_
#define GP_FULL_H_

#define _USE_MATH_DEFINES
#include <cmath>
#include <Eigen/Dense>

#include "gp.h"
#include "optimizer.h"

namespace libgp
{

/** Full gaussian process regression.
*  @author Manuel Blum */
class FullGaussianProcess : public GaussianProcess
{
public:

	/** Create and instance of FullGaussianProcess with given input dimensionality
	 *  and covariance function. */
	FullGaussianProcess(size_t input_dim, std::string covf_def);

	/** Create and instance of FullGaussianProcess from file. */
	FullGaussianProcess(const char * filename);

	virtual ~FullGaussianProcess();

    /** Predict target value for given input.
     *  @param x input vector
     *  @return predicted value */
    virtual double f(const double x[]);

    /** Predict variance of prediction for given input.
     *  @param x input vector
     *  @return predicted variance */
    virtual double var(const double x[]);

    /** Add input-output-pair to sample set.
     *  Add a copy of the given input-output-pair to sample set.
     *  @param x input array
     *  @param y output value
     */
    virtual void add_pattern(const double x[], double y);

    bool virtual set_y(size_t i, double y);

    double log_likelihood();

    Eigen::VectorXd log_likelihood_gradient();

    virtual void optimize(Optimizer* opt, size_t n=100, bool verbose=1);

protected:

    /** Alpha is cached for performance. */
    Eigen::VectorXd alpha;

    /** Last test kernel vector. */
    Eigen::VectorXd k_star;

    /** Linear solver used to invert the covariance matrix. */
//    Eigen::LLT<Eigen::MatrixXd> solver;
    Eigen::MatrixXd L;

    /** Update test input and cache kernel vector. */
	void update_k_star(const Eigen::VectorXd &x_star);

	void update_alpha();

	/** Compute covariance matrix and perform cholesky decomposition. */
	virtual void compute();

	bool alpha_needs_update;
};

} /* namespace libgp */
#endif /* GP_FULL_H_ */
