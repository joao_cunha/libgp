/*
 * gp_local.h
 *
 *  Created on: Oct 3, 2013
 *      Author: alex
 */

#ifndef GP_LOCAL_H_
#define GP_LOCAL_H_

#include "gp.h"
#include "gp_full.h"
#include "cov_factory.h"


#include <map>
#include <list>
#include <fstream>
#include <deque>

namespace libgp
{

struct ClusterDataSet
{
	std::deque<unsigned int> samplesIdx;
	Eigen::VectorXd centroid;
};

struct Cluster
{
	Eigen::VectorXd centroid;
	FullGaussianProcess* gp;
	Cluster(Eigen::VectorXd centroid, FullGaussianProcess* gp) : centroid(centroid), gp(gp) {}
//	~Cluster(){delete gp;}
};

struct LocalMapping
{
	unsigned int gpIdx;
	unsigned int localIdx;
	LocalMapping() : gpIdx(0), localIdx(0) {}
	LocalMapping(unsigned int gpIdx, unsigned int localIdx) : gpIdx(gpIdx), localIdx(localIdx) {}
};

class Pair
{

public:
	Pair(const double& rDist, const unsigned int& idx): rDist(rDist), idx(idx){}

	double rDist;
	unsigned int idx;
};

enum NoiseType {None = 0, Noise, Random};

class LocalGaussianProcess : public GaussianProcess
{
public:
	LocalGaussianProcess(size_t input_dim, std::string covf_def, size_t maxSize, NoiseType n, double noise_coef = 0);
	LocalGaussianProcess(const char * filename, unsigned int maxSize, NoiseType n, double noise_coef = 0);
	virtual ~LocalGaussianProcess();

	virtual double f(const double x[]);
	virtual double var(const double x[]);
	virtual bool set_y(size_t i, double y);
	virtual void add_pattern(const double x[], double y);

	unsigned int get_num_clusters();
	Cluster get_cluster(unsigned int i);
	virtual void clear_sampleset();

    virtual void optimize(Optimizer* opt, size_t n=100, bool verbose=1);

protected:
	std::map <unsigned int, LocalMapping > mapping;
	std::vector<Cluster> localGPs;
	std::string covf_def;
	size_t maxSize;
	bool clusters_need_update;
	NoiseType n;
	double noise_coef;


	 virtual void compute();
	 void update_hyper_param();

	 std::list<ClusterDataSet> clustering(const std::deque<unsigned int>& samplesIdx, unsigned int& k);
	 std::vector<Eigen::VectorXd> kmeansInit(const std::deque<unsigned int>& samplesIdx, const unsigned int& k, const Eigen::MatrixXd & noise);
	 void maximize(const std::deque<unsigned int>& samples, Eigen::VectorXd& centroid);
	 void clearLocalGPs();

	std::vector<Pair> updateOptics(const std::deque<unsigned int>& samplesIdx, const unsigned int & minPts, const double & eps);
	std::vector<unsigned int> getEpsNeighborhood(const std::deque<unsigned int>& samplesIdx, const double & eps, const unsigned int & idx);
	double getCoreDistance(const std::deque<unsigned int>& samplesIdx, const unsigned int & minPts, const double & eps, const unsigned int & idx);
	double getReachabilityDistance(const std::deque<unsigned int>& samplesIdx, const unsigned int& idx1, const unsigned int& idx2, const double& core_dist);
	void updateReachabilityList(std::map<unsigned int, double>& reachability, const std::deque<unsigned int>& samplesIdx, const unsigned int& idx, const std::vector<unsigned int>& neighbors, const double& cDist, const std::map<unsigned int, bool>& processed);
};

} /* namespace libgp */
#endif /* GP_LOCAL_H_ */
