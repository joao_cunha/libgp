/*
 * optimizer.h#include "gp_full.h"

 *
 *  Created on: Mar 10, 2014
 *      Author: alex
 */

#ifndef OPTIMIZER_H_
#define OPTIMIZER_H_

#include <stdlib.h>

namespace libgp
{

class FullGaussianProcess; //forward declaration

class Optimizer
{
public:
	virtual ~Optimizer(){};
	virtual void maximize(FullGaussianProcess* gp, size_t n, bool verbose) = 0;
};

} /* namespace libgp */
#endif /* OPTIMIZER_H_ */
