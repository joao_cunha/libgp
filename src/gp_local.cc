/*
 * gp_local.cc
 *
 *  Created on: Oct 3, 2013
 *      Author: alex
 */

#include "gp_local.h"

#include "gp_utils.h"

#include <cmath>
#include <limits>
#include <sys/time.h>
#include <time.h>
#include <fstream>

using namespace std;

namespace libgp
{

LocalGaussianProcess::LocalGaussianProcess(size_t input_dim, std::string covf_def, size_t maxSize, NoiseType n, double noise_coef)
	: GaussianProcess(input_dim, covf_def), covf_def(covf_def), maxSize(maxSize), clusters_need_update(true), n(n), noise_coef(noise_coef)
{
}

LocalGaussianProcess::LocalGaussianProcess(const char* filename, unsigned int maxSize, NoiseType n, double noise_coef)
	: maxSize(maxSize), clusters_need_update(true), n(n), noise_coef(noise_coef)
{
	int stage = 0;
	std::ifstream infile;
	double y;
	infile.open(filename);
	std::string s;
	double * x = NULL;
	while (infile.good()) {
	  getline(infile, s);
	  // ignore empty lines and comments
	  if (s.length() != 0 && s.at(0) != '#') {
		std::stringstream ss(s);
		if (stage > 2) {
		  ss >> y;
		  for(size_t j = 0; j < input_dim; ++j) {
			ss >> x[j];
		  }
		  add_pattern(x, y);
		} else if (stage == 0) {
		  ss >> input_dim;
		  sampleset = new SampleSet(input_dim);
		  x = new double[input_dim];
		} else if (stage == 1) {
		  CovFactory factory;
		  cf = factory.create(input_dim, s);
		  cf->loghyper_changed = 0;
		  covf_def = s;
		} else if (stage == 2) {
		  Eigen::VectorXd params(cf->get_param_dim());
		  for (size_t j = 0; j<cf->get_param_dim(); ++j) {
			ss >> params[j];
		  }
		  cf->set_loghyper(params);
		}
		stage++;
	  }
	}
	infile.close();
	if (stage < 3) {
	  std::cerr << "fatal error while reading " << filename << std::endl;
	  exit(EXIT_FAILURE);
	}
	delete [] x;

	if(sampleset->size() > 0)
		compute();
}

LocalGaussianProcess::~LocalGaussianProcess()
{
	for (unsigned int i = 0; i < localGPs.size(); ++i)
	{
		delete localGPs[i].gp;
	}
}

double LocalGaussianProcess::f(const double x[])
{
	if (sampleset->empty()) return 0;
	update_hyper_param();
	compute();

//	Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
//	double normalizer = 0, sum = 0;
//	for (unsigned int i = 0; i < localGPs.size(); ++i)
//	{
//		FullGaussianProcess* gp = localGPs[i].gp;
//		Eigen::VectorXd center = localGPs[i].centroid;
//
//		double cov = 1;// gp->covf().get(center, x_star);
//		normalizer += cov / gp->var(x);
//		sum += gp->f(x) * cov / gp->var(x);
//	}
//
//	return sum / normalizer;

	double mean = localGPs[0].gp->f(x);
	double var = localGPs[0].gp->var(x);

	for (unsigned int i = 1; i < localGPs.size(); ++i)
	{
		FullGaussianProcess* gp = localGPs[i].gp;

		double tmpMean = gp->f(x);
		double tmpVar = gp->var(x);

		mean = (mean*tmpVar + tmpMean*var)/(var+tmpVar);
		var = 1/(1/var+1/tmpVar);
	}

	return mean;

}

double LocalGaussianProcess::var(const double x[])
{
	if (sampleset->empty()) return 0;
	update_hyper_param();
	compute();

//	//Harmonic mean
//	double sum = 0;
//	for (unsigned int i = 0; i < localGPs.size(); ++i)
//	{
//		FullGaussianProcess* gp = localGPs[i].gp;
//		sum += 1 / gp->var(x);
//	}
//
//	return localGPs.size() * sum;

	double var = localGPs[0].gp->var(x);

	for (unsigned int i = 1; i < localGPs.size(); ++i)
	{
		FullGaussianProcess* gp = localGPs[i].gp;

		double tmpVar = gp->var(x);

		var = 1/(1/var+1/tmpVar);
	}

	return var;

}

bool LocalGaussianProcess::set_y(size_t i, double y)
{
	if(GaussianProcess::set_y(i, y)) //update sampleset
	{
		if(mapping.count(i))
		{
			unsigned int gpIdx = mapping.at(i).gpIdx;
			unsigned int localIdx = mapping.at(i).localIdx;
			localGPs[gpIdx].gp->set_y(localIdx, y);
			return true;
		}
		return false;
	}
	return false;
}

void LocalGaussianProcess::compute()
{
	if(clusters_need_update == false)
		return;

	//clear existing local GPs
	clearLocalGPs();

	struct timeval tv1, tv2;
	double dur;

	gettimeofday( &tv1 , NULL );

	std::cout << "COMPUTE" << std::endl;

	unsigned int n = sampleset->size();

	//calc k
	unsigned int k = n==0 ? 1 : ceil((double)n/maxSize);

	std::cout << "FIRST CLUSTERING CALL, for " << k << " clusters" << std::endl;

	std::deque<unsigned int> samplesIdx;
	for (unsigned int i = 0; i < sampleset->size(); ++i)
	{
		samplesIdx.push_back(i);
	}

	//kmeans++ on entire dataset
	std::list<ClusterDataSet> clusters = clustering(samplesIdx, k);

	std::cout << "END FIRST CLUSTERING CALL" << std::endl;

	for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
	{
		ClusterDataSet c = *it;

		std::cout << "Cluster size " << c.samplesIdx.size() << std::endl;

		if(c.samplesIdx.size() > maxSize)
		{
			k = ceil((double)c.samplesIdx.size() / maxSize);
			std::cout << "MORE CLUSTERING CALLS, k = " << k  << std::endl;
			std::list<ClusterDataSet> tmp = clustering(c.samplesIdx, k);

			std::cout << "adding " << tmp.size() << " more clusters" << std::endl;

			while(tmp.empty() == false)
			{
				std::cout << "adding cluster with size " << tmp.front().samplesIdx.size() << std::endl;
				clusters.push_back(tmp.front());
				tmp.pop_front();
			}

			it = clusters.erase(it);
			it--; //go back
		}
	}

	gettimeofday( &tv2 , NULL );

	dur = (tv2.tv_sec*1000.0 + tv2.tv_usec/1000.0) - (tv1.tv_sec*1000.0 + tv1.tv_usec/1000.0);

	std::cout << "Time clustering = " << dur << std::endl;

//	std::cout << "AFTER size checking" << std::endl;

	for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
	{
		if(it->samplesIdx.size() > 0)
		{
			FullGaussianProcess* gp = new FullGaussianProcess(input_dim, covf_def);
			gp->covf().set_loghyper(cf->get_loghyper()); //set global covf hyperparameters

			unsigned int n = localGPs.size();

			for (unsigned int i = 0; i < it->samplesIdx.size(); ++i) //for each sample of this cluster
			{
				unsigned int globalIdx = it->samplesIdx[i];
				gp->add_pattern(sampleset->x(globalIdx).data(), sampleset->y(globalIdx));
				mapping[globalIdx] = LocalMapping(n, gp->get_sampleset_size()-1);
			}

			localGPs.push_back(Cluster(it->centroid, gp));
		}
	}

//	std::cout << "Number of local GPs = " << localGPs.size() << std::endl;
//	for (unsigned int i = 0; i < localGPs.size(); ++i)
//	{
//		std::cout << "Local GP " << i << " with size " << localGPs[i].gp->get_sampleset_size() << std::endl;
//	}

	clusters_need_update = false;

//	std::cout << "END COMPUTE" << std::endl;

//	std::vector<Pair> output = updateOptics(samplesIdx, 5, cf->get(sampleset->x(0), sampleset->x(0))*0.5);
//
//	std::ofstream ofs("optics.txt", std::ofstream::trunc);
//
//	for (unsigned int i = 0; i < output.size(); ++i)
//	{
//		ofs << output[i].rDist << " " << output[i].idx << std::endl;
//	}
//
//	ofs.close();
}

void LocalGaussianProcess::update_hyper_param()
{
	if(cf->loghyper_changed == false)
		return;

	cf->loghyper_changed = false;

	for (unsigned int i = 0; i < localGPs.size(); ++i)
	{
		localGPs[i].gp->cf->set_loghyper(cf->get_loghyper());
	}

}

void LocalGaussianProcess::add_pattern(const double x[], double y)
{
	GaussianProcess::add_pattern(x, y);
	clusters_need_update = true;
}

std::list<ClusterDataSet> LocalGaussianProcess::clustering(const std::deque<unsigned int>& samplesIdx, unsigned int& k)
{

	//create noise matrix
	Eigen::MatrixXd noise = Eigen::MatrixXd::Zero(input_dim, samplesIdx.size());

	//Initialize clusters
	std::list<ClusterDataSet> clusters;

//	double scale = 1;

	/**
	 * two loops
	 * first loop clusters without noise
	 * second loop applies noise and reclusters the samples
	 */
	for (int loop = 0; loop < 2; ++loop)
	{
		std::vector<Eigen::VectorXd> centers = kmeansInit(samplesIdx, k, noise);

		clusters.clear();

		for (unsigned int i = 0; i < centers.size(); ++i)
		{
			ClusterDataSet c;
			c.centroid = centers[i];
			clusters.push_back(c);
		}

		cout << "loop = " << loop << endl;

		//k-means
		std::vector<Eigen::VectorXd> sum(k, Eigen::VectorXd::Zero(input_dim));
		std::vector<double> normalizer(k, 0);

		double maxChange;
		double epsilon = 0.001;

		do
		{
			// Another clustering iteration
			maxChange = -std::numeric_limits<double>::max();

			//clear sampleset
			for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
			{
				it->samplesIdx.clear();
			}

			//for each input point get closest cluster centroid
			for (unsigned int i = 0; i < samplesIdx.size(); ++i)
			{
//				double maxCov = -std::numeric_limits<double>::max();
				double minDist = std::numeric_limits<double>::max();

				int closestIdx = -1;
				std::list<ClusterDataSet>::iterator closestCluster;

				unsigned int globalIdx = samplesIdx[i];

				int j = 0;

				for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
				{
//					double cov = cf->get(sampleset->x(globalIdx) + noise.col(i), it->centroid)*scale;
//					if(cov > maxCov)
//					{
//						maxCov = cov;
//						closestIdx = j;
//						closestCluster = it;
//					}

					double dist = (sampleset->x(globalIdx) + noise.col(i) - it->centroid).norm();
					if(dist < minDist)
					{
						minDist = dist;
						closestIdx = j;
						closestCluster = it;
					}

					j++;
				}

				//assign the point to the closest cluster
				closestCluster->samplesIdx.push_back(globalIdx);

//				sum[closestIdx] = sum[closestIdx] + (sampleset->x(globalIdx) + noise.col(i)) * maxCov;
//				normalizer[closestIdx] += maxCov;

				sum[closestIdx] = sum[closestIdx] + (sampleset->x(globalIdx) + noise.col(i));
				normalizer[closestIdx]++;

			}

			//Update the centroids
			int j = 0;
			for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
			{
				Eigen::VectorXd tmp = sum[j]/normalizer[j];

				//choose centroid that maximizes the sum of cov
//				Eigen::VectorXd tmp = it->centroid;
//				cout << "centroid before maximization " << tmp.transpose() << endl ;
//				maximize(it->samplesIdx, tmp);
//				cout << "centroid after maximization " << tmp.transpose() << endl;


				double change = (tmp-it->centroid).norm();
				maxChange = std::max(maxChange, change);

				it->centroid = tmp;

				sum.at(j) = Eigen::VectorXd::Zero(input_dim);
				normalizer[j] = 0.0;

				j++;

			}

			cout << "Max Change = " << maxChange<< endl;

		}while(maxChange > epsilon);

		if (loop > 0) //no need to recalculate noise after the first loop
			break;

		//get largest intra-cluster dist value
		double maxDist = -std::numeric_limits<double>::max();
		for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
		{
			for (unsigned int i = 0; i < it->samplesIdx.size(); ++i)
			{
				unsigned int globalIdx = it->samplesIdx.at(i);
				double dist = (sampleset->x(globalIdx)-it->centroid).norm();
				if(dist > maxDist)
					maxDist = dist;
			}
		}

		cout << endl << "Max distance = " << maxDist << endl << endl;

		switch (n)
		{
			case Noise:
				for (unsigned int i = 0; i < samplesIdx.size(); ++i)
				{
					for (unsigned int j = 0; j < input_dim; ++j)
					{
						noise(j,i) = Utils::randn()*maxDist*noise_coef;
					}
				}

//				scale = maxDist*noise_coef;
				break;

			case Random:
				noise = noise.setRandom(input_dim, samplesIdx.size())*10;
				break;

			default:
				break;

		}

//		cout << noise << endl;

	}

	//recalculate centroids without noise
	for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
	{
		Eigen::VectorXd centroid = Eigen::VectorXd::Zero(input_dim);
		double normalizer = 0;

		for (unsigned int i = 0; i < it->samplesIdx.size(); ++i)
		{
			unsigned int globalIdx = it->samplesIdx.at(i);
			double cov = cf->get(sampleset->x(globalIdx), it->centroid);

//			centroid = centroid + sampleset->x(globalIdx)*cov;
			centroid = centroid + sampleset->x(globalIdx);
			normalizer += cov;
		}

//		it->centroid = centroid/normalizer;
		it->centroid = centroid/it->samplesIdx.size();
	}


	//calculate closest centroid cluster with noise
//	for (unsigned int i = 0; i < samplesIdx.size(); ++i)
//	{
//		double maxCov = -std::numeric_limits<double>::max();
//		std::list<ClusterDataSet>::iterator closestCluster;
//
//		unsigned int globalIdx = samplesIdx[i];
//
//		int j = 0;
//
//		//update the closest cluster
//		for (std::list<ClusterDataSet>::iterator it = clusters.begin(); it != clusters.end(); it++)
//		{
//			double cov = cf->get(sampleset->x(globalIdx) + noise.col(i), it->centroid);// + noise(i,j);
//			if(cov > maxCov)
//			{
//				maxCov = cov;
//				closestCluster = it;
//			}
//			j++;
//		}
//
//		//assign the point to the closest cluster
//		closestCluster->samplesIdx.push_back(globalIdx);
//	}


	std::cout << "End clustering" << std::endl;

	return clusters;
}

std::vector<Eigen::VectorXd> LocalGaussianProcess::kmeansInit(const std::deque<unsigned int>& samplesIdx, const unsigned int& k, const Eigen::MatrixXd& noise)
{
	//call srand
	struct timeval tv;
	gettimeofday( &tv , NULL );
	long unsigned int seed = tv.tv_sec*1000+tv.tv_usec/1000;

	srand(seed);

	std::vector<Eigen::VectorXd> centers;

	//k-means++ initialization
	unsigned int index = rand() % samplesIdx.size();

//	cout << "Index = " << index << endl;
	centers.push_back(sampleset->x(samplesIdx[index]) + noise.col(index)); // random first center


	for (unsigned int i = 0; i < k-1; ++i) //for every remaining cluster
	{
//		cout << "Picking centroid " << i+2 << endl;
		std::vector<double> distances;

		double maxDist = -std::numeric_limits<double>::max();

		for (unsigned int j = 0; j < samplesIdx.size(); ++j) //for every point
		{
			double minDist = std::numeric_limits<double>::max();

			for (unsigned int l = 0; l < centers.size(); ++l) //for all already chosen centroids
			{
				double dist = (sampleset->x(samplesIdx[j])+noise.col(j) - centers[l]).norm();
				minDist = std::min(minDist, dist);
			}

			maxDist = std::max(maxDist, minDist);

			distances.push_back(minDist);

		}

		double beta = drand48() * (2*maxDist);

//		cout << "MaxDist = " << maxDist << endl;
//		cout << "Beta = " << beta << endl;


		while(beta > distances[index] || distances[index]==0)
		{
			beta -= distances[index];
			index = (index+1) % samplesIdx.size();
		}

		centers.push_back(sampleset->x(samplesIdx[index]) + noise.col(index));
	}

	return centers;
}

void LocalGaussianProcess::maximize(const std::deque<unsigned int>& samples, Eigen::VectorXd& centroid)
{

	double eps_stop = 0.0;
	double Delta0 = 0.1;
	double Deltamin=1e-6;
	double Deltamax=50;
	double etaminus=0.5;
	double etaplus=1.2;
	unsigned int nIter=50;

	//int input_dim = gp->covf().get_param_dim();
	Eigen::VectorXd Delta = Eigen::VectorXd::Ones(input_dim) * Delta0;
	Eigen::VectorXd grad_old = Eigen::VectorXd::Zero(input_dim);
	Eigen::VectorXd params = centroid;
	Eigen::VectorXd best_params = centroid;
	double best = -std::numeric_limits<double>::max();

	for (size_t i=0; i<nIter; ++i)
	{
		double sumCov = 0;
		Eigen::VectorXd grad = Eigen::VectorXd::Zero(input_dim);
		Eigen::VectorXd g = grad;

		for (unsigned int j = 0; j < samples.size(); ++j)
		{
			unsigned int globalIdx = samples[j];
			sumCov += cf->get(centroid, sampleset->x(globalIdx));
			cf->gradX1(centroid, sampleset->x(globalIdx),g);
			grad+=g;
//			cout << "g[" << j << "] = " << g << endl;
		}

//		cout << "grad = " << g.transpose() << endl;

//		if (verbose) std::cout << i << " " << sumCov << std::endl;

		grad_old = grad_old.cwiseProduct(grad);
		for (int j=0; j<grad_old.size(); ++j) {
			if (grad_old(j) > 0) {
				Delta(j) = std::min(Delta(j)*etaplus, Deltamax);
			} else if (grad_old(j) < 0) {
				Delta(j) = std::max(Delta(j)*etaminus, Deltamin);
				grad(j) = 0;
			}
			params(j) += Utils::sign(grad(j)) * Delta(j);
		}
		grad_old = grad;
		if (grad_old.norm() < eps_stop) break;
		centroid = params;
		if (sumCov > best) {
			best = sumCov;
			best_params = params;
		}
	}
	centroid = best_params;
}



unsigned int LocalGaussianProcess::get_num_clusters()
{
	return localGPs.size();
}

Cluster LocalGaussianProcess::get_cluster(unsigned int i)
{
	return localGPs[i];
}

void LocalGaussianProcess::clear_sampleset()
{
	GaussianProcess::clear_sampleset();
	clearLocalGPs();
}

void LocalGaussianProcess::optimize(Optimizer* opt, size_t n, bool verbose)
{
	compute();

	for (unsigned int i = 0; i < localGPs.size(); ++i)
	{
		cout << "Maximizing GP " << i << endl;
		opt->maximize(localGPs[i].gp, n, verbose);
	}
}

void LocalGaussianProcess::clearLocalGPs()
{
	for (unsigned int i = 0; i < localGPs.size(); ++i)
	{
		delete localGPs[i].gp;
	}
	localGPs.clear();
	mapping.clear();
}

std::vector<Pair> LocalGaussianProcess::updateOptics(
		const std::deque<unsigned int>& samplesIdx, const unsigned int& minPts,
		const double& eps)
{
	std::vector<Pair> orderedList;

	std::map<unsigned int, bool> processed;
	for (unsigned int i = 0; i < samplesIdx.size(); ++i)
	{
		unsigned int idx = samplesIdx[i];
		processed[idx] = false;
	}

	for (unsigned int i = 0; i < samplesIdx.size(); ++i)
	{
		//get idx from i
		unsigned int idx = samplesIdx[i];

		if(processed[idx])
			continue;

		std::vector<unsigned int> neighbors = getEpsNeighborhood(samplesIdx, eps, i);
		processed[idx] = true;

//		orderedList.push_back(Pair(std::numeric_limits<double>::max(), idx)); //use for distance based optics

		std::cout << "processing " << idx << " sample" << std::endl;

		orderedList.push_back(Pair(-std::numeric_limits<double>::max(), idx)); //use for cov based optics

		std::map<unsigned int, double> reachability;

		double cDist = getCoreDistance(samplesIdx, minPts, eps, idx); //pass neighbors

//		if (cDist != std::numeric_limits<double>::max()) //use for distance based optics
		if (cDist != -std::numeric_limits<double>::max()) //use for cov based optics
		{
			std::cout << "processing neighbors " << neighbors.size() << std::endl;

			updateReachabilityList(reachability, samplesIdx, idx, neighbors, cDist, processed);

			while(reachability.empty() == false)
			{
				std::cout << "Reachability size " << reachability.size() << std::endl;

				unsigned int newIdx = -1;

				//find minimum for distance based optics
//				double minDist = std::numeric_limits<double>::max();

				//find maximum for cov based optics
				double maxCov = -std::numeric_limits<double>::max();

				for(std::map<unsigned int, double>::iterator it = reachability.begin(); it != reachability.end(); it++)
				{
					//use for distance based optics
//					if(it->second < minDist)
//					{
//						minDist = it->second;
//						newIdx = it->first;
//					}

					//use for distance based optics
					if(it->second > maxCov)
					{
						maxCov = it->second;
						newIdx = it->first;
					}
				}

				std::cout << "newIdx = " << newIdx << std::endl;

				std::vector<unsigned int> moreNeighbors = getEpsNeighborhood(samplesIdx, eps, newIdx);
				processed[newIdx] = true;

				std::cout << "processing " << newIdx << " sample" << std::endl;

				orderedList.push_back(Pair(reachability[newIdx], newIdx));

				std::cout << "Erasing " << newIdx << std::endl;
				reachability.erase(newIdx);

				for(std::map<unsigned int, double>::iterator it = reachability.begin(); it != reachability.end(); it++)
				{
					std::cout << "reachability after erase " << it->first << " -> " << it->second << std::endl;
				}

				double cNewDist = getCoreDistance(samplesIdx, minPts, eps, newIdx);

//				if (cNewDist < std::numeric_limits<double>::max()) //use for distance based optics
				if (cNewDist > -std::numeric_limits<double>::max()) //use for cov based optics
				{
					std::cout << "processing more neighbors " << moreNeighbors.size() << std::endl;
					updateReachabilityList(reachability, samplesIdx, newIdx, moreNeighbors, cNewDist, processed);
				}
			}
		}
	}


	return orderedList;
}

std::vector<unsigned int> LocalGaussianProcess::getEpsNeighborhood(
		const std::deque<unsigned int>& samplesIdx, const double& eps,
		const unsigned int& idx)
{
	std::vector<unsigned int> neighbors;

	for (unsigned int i = 0; i < samplesIdx.size(); ++i)
	{
		unsigned int tmpIdx = samplesIdx[i];

		//use for distance based optics
//		double dist = (sampleset->x(idx) - sampleset->x(tmpIdx)).norm();
//
//		if(dist < eps) //later use cov
//		{
//			neighbors.push_back(tmpIdx);
//		}

		//use for cov based optics
		double cov = cf->get(sampleset->x(idx), sampleset->x(tmpIdx));

		if(cov > eps)
		{
			neighbors.push_back(tmpIdx);
		}
	}

	return neighbors;
}

double LocalGaussianProcess::getCoreDistance(
		const std::deque<unsigned int>& samplesIdx, const unsigned int& minPts,
		const double& eps, const unsigned int& idx)
{
	//use for distance based optics
//	std::vector<unsigned int> neighbors = getEpsNeighborhood(samplesIdx, eps, idx);
//	if(neighbors.size() < minPts)
//		return std::numeric_limits<double>::max(); //later set to zero
//
//	std::list<double> minPtsDist(minPts, std::numeric_limits<double>::max()); //later set to zero or -inf

	//use for cov based optics
	std::vector<unsigned int> neighbors = getEpsNeighborhood(samplesIdx, eps, idx);
	if(neighbors.size() < minPts)
		return -std::numeric_limits<double>::max();

	std::list<double> minPtsDist(minPts, -std::numeric_limits<double>::max()); //later set to zero or -inf

	for (unsigned int i = 0; i < neighbors.size(); ++i)
	{
		unsigned int tmpIdx = neighbors[i];

//		double dist = (sampleset->x(idx) - sampleset->x(tmpIdx)).norm(); //use for distance based optics

		double cov = cf->get(sampleset->x(idx), sampleset->x(tmpIdx)); //use for cov based optics

		for(std::list<double>::iterator it = minPtsDist.begin(); it!= minPtsDist.end(); it++)
		{
//			if(dist < *it) //use for distance based optics
			if(cov > *it) //use for cov based optics
			{
//				minPtsDist.insert(it,dist); //use for distance based optics
				minPtsDist.insert(it,cov); //use for cov based optics

				minPtsDist.pop_back();
				break;
			}
		}
	}

	return minPtsDist.back();
}

double LocalGaussianProcess::getReachabilityDistance(
		const std::deque<unsigned int>& samplesIdx, const unsigned int& idx1,
		const unsigned int& idx2, const double& core_dist)
{
//	double dist = (sampleset->x(idx1) - sampleset->x(idx2)).norm(); //use for distance based optics
//	return max(core_dist, dist);

	double cov = cf->get(sampleset->x(idx1), sampleset->x(idx2)); //use for cov based optics
	return min(core_dist, cov);
}

void LocalGaussianProcess::updateReachabilityList(
		std::map<unsigned int, double>& reachability,
		const std::deque<unsigned int>& samplesIdx, const unsigned int& idx,
		const std::vector<unsigned int>& neighbors, const double& cDist,
		const std::map<unsigned int, bool>& processed)
{
	for(std::map<unsigned int, double>::iterator it = reachability.begin(); it != reachability.end(); it++)
	{
		std::cout << "reachability before update " << it->first << " -> " << it->second << std::endl;
	}


	for (unsigned int i = 0; i < neighbors.size(); ++i)
	{
		unsigned int neighborIdx = neighbors[i];
		double rDist = getReachabilityDistance(samplesIdx, idx, neighborIdx,
				cDist);

		if(neighborIdx == 0 )
			std::cout << "updating zero" << std::endl;

		if (processed.at(neighborIdx))
		{
			std::cout << "Skipping " << neighborIdx << std::endl;
			continue;
		}

		if(neighborIdx == 0 )
			std::cout << "after processed check" << std::endl;

		if (reachability.count(neighborIdx))
//			if (reachability[neighborIdx] < rDist) //use for distance based optics
			if (reachability[neighborIdx] > rDist) //use for cov based optics
				rDist = reachability[neighborIdx];

		std::cout << "adding " << neighborIdx << " -> " << rDist << std::endl;
		reachability[neighborIdx] = rDist;

		if(neighborIdx == 0 )
			std::cout << "0 in reachability list" << std::endl;
	}

	for(std::map<unsigned int, double>::iterator it = reachability.begin(); it != reachability.end(); it++)
	{
		std::cout << "reachability after update " << it->first << " -> " << it->second << std::endl;
	}

}

} /* namespace libgp */
