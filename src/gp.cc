// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "gp.h"
#include "cov_factory.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <ctime>

namespace libgp {
  
  const double log2pi = log(2*M_PI);
  const double initial_L_size = 1000;

  GaussianProcess::GaussianProcess ()
  {

  }

  GaussianProcess::GaussianProcess (size_t input_dim, std::string covf_def)
  {
    // set input dimensionality
    this->input_dim = input_dim;
    // create covariance function
    CovFactory factory;
    cf = factory.create(input_dim, covf_def);
    cf->loghyper_changed = 0;
    sampleset = new SampleSet(input_dim);
  }
  
  GaussianProcess::~GaussianProcess ()
  {
    // free memory
    delete sampleset;
    delete cf;
  }  
  
  void GaussianProcess::write(const char* filename)
  {
	// output
	std::ofstream outfile;
	outfile.open(filename);
	time_t curtime = time(0);
	tm now=*localtime(&curtime);
	char dest[BUFSIZ]= {0};
	strftime(dest, sizeof(dest)-1, "%c", &now);
	outfile << "# " << dest << std::endl << std::endl
	<< "# input dimensionality" << std::endl << input_dim << std::endl
	<< std::endl << "# covariance function" << std::endl
	<< cf->to_string() << std::endl << std::endl
	<< "# log-hyperparameter" << std::endl;
	Eigen::VectorXd param = cf->get_loghyper();
	for (size_t i = 0; i< cf->get_param_dim(); i++) {
	  outfile << std::setprecision(10) << param(i) << " ";
	}
	outfile << std::endl << std::endl
	<< "# data (target value in first column)" << std::endl;
	for (size_t i=0; i<sampleset->size(); ++i) {
	  outfile << std::setprecision(10) << sampleset->y(i) << " ";
	  for(size_t j = 0; j < input_dim; ++j) {
		outfile << std::setprecision(10) << sampleset->x(i)(j) << " ";
	  }
	  outfile << std::endl;
	}
	outfile.close();
  }

  void GaussianProcess::add_pattern(const double x[], double y)
  {
    sampleset->add(x, y);
  }

  bool GaussianProcess::set_y(size_t i, double y)
  {
    if(sampleset->set_y(i,y)) {
      return 1;
    }
    return false;
  }

  size_t GaussianProcess::get_sampleset_size()
  {
    return sampleset->size();
  }
  
  void GaussianProcess::clear_sampleset()
  {
    sampleset->clear();
  }
  
  CovarianceFunction & GaussianProcess::covf()
  {
    return *cf;
  }

  size_t GaussianProcess::get_input_dim()
  {
    return input_dim;
  }
}
