// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.


#include <cmath>
#include <iostream>

#include "rprop.h"
#include "gp_utils.h"

namespace libgp {

void RProp::init(double eps_stop, double Delta0, double Deltamin, double Deltamax, double etaminus, double etaplus)
{
  this->Delta0   = Delta0;
  this->Deltamin = Deltamin;
  this->Deltamax = Deltamax;
  this->etaminus = etaminus;
  this->etaplus  = etaplus;
  this->eps_stop = eps_stop;

}

void RProp::maximize(FullGaussianProcess * gp, size_t n, bool verbose)
{
  int param_dim = gp->covf().get_param_dim();
  Eigen::VectorXd Delta = Eigen::VectorXd::Ones(param_dim) * Delta0;
  Eigen::VectorXd grad_old = Eigen::VectorXd::Zero(param_dim);
  Eigen::VectorXd params = gp->covf().get_loghyper();
  Eigen::VectorXd old_params = params;
  Eigen::VectorXd best_params = params;
  double best = log(0);

  for (size_t i=0; i<n; ++i) {
    double lik = gp->log_likelihood();
    old_params = params;
    if (verbose) std::cout << i << " " << -lik << " " << params.transpose() << std::endl << gp->covf().get_loghyper().transpose() << std::endl;
    Eigen::VectorXd grad = -gp->log_likelihood_gradient();
    grad_old = grad_old.cwiseProduct(grad);
    for (int j=0; j<grad_old.size(); ++j) {
      if (grad_old(j) > 0) {
        Delta(j) = std::min(Delta(j)*etaplus, Deltamax);        
      } else if (grad_old(j) < 0) {
        Delta(j) = std::max(Delta(j)*etaminus, Deltamin);
        grad(j) = 0;
      } 
      params(j) += -Utils::sign(grad(j)) * Delta(j);
    }
    grad_old = grad;
    if (grad_old.norm() < eps_stop) break;
    gp->covf().set_loghyper(params);
    if (lik > best) {
      if (verbose) std::cout << "best hyperparameters found = " << old_params.transpose() << std::endl;
      best = lik;
      best_params = old_params;
    }
  }


  if (verbose) std::cout << "setting hyperparameters = " << best_params.transpose() << std::endl;

  gp->covf().set_loghyper(best_params);

  Eigen::VectorXd x = Eigen::VectorXd::Zero(gp->get_input_dim());

  //This will call compute
  gp->f(x.data());
}

}


