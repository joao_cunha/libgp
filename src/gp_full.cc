/*
 * gp_full.cpp
 *
 *  Created on: Sep 27, 2013
 *      Author: alex
 */

#include "gp_full.h"
#include "cov_factory.h"

#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <ctime>

namespace libgp
{

const double log2pi = log(2*M_PI);
const int initial_L_size = 1000;

FullGaussianProcess::FullGaussianProcess(size_t input_dim, std::string covf_def)
	: GaussianProcess(input_dim, covf_def), alpha_needs_update(false)
{
	L.resize(initial_L_size, initial_L_size);
}

FullGaussianProcess::FullGaussianProcess(const char* filename)
{
    int stage = 0;
    std::ifstream infile;
    double y;
    infile.open(filename);
    std::string s;
    double * x = NULL;
    L.resize(initial_L_size, initial_L_size);
    while (infile.good()) {
      getline(infile, s);
      // ignore empty lines and comments
      if (s.length() != 0 && s.at(0) != '#') {
        std::stringstream ss(s);
        if (stage > 2) {
          ss >> y;
          for(size_t j = 0; j < input_dim; ++j) {
            ss >> x[j];
          }
          add_pattern(x, y);
        } else if (stage == 0) {
          ss >> input_dim;
          sampleset = new SampleSet(input_dim);
          x = new double[input_dim];
        } else if (stage == 1) {
          CovFactory factory;
          cf = factory.create(input_dim, s);
          cf->loghyper_changed = 0;
        } else if (stage == 2) {
          Eigen::VectorXd params(cf->get_param_dim());
          for (size_t j = 0; j<cf->get_param_dim(); ++j) {
            ss >> params[j];
          }
          cf->set_loghyper(params);
        }
        stage++;
      }
    }
    infile.close();
    if (stage < 3) {
      std::cerr << "fatal error while reading " << filename << std::endl;
      exit(EXIT_FAILURE);
    }
    delete [] x;
}

FullGaussianProcess::~FullGaussianProcess()
{
}

double FullGaussianProcess::f(const double x[])
{
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    return k_star.dot(alpha);
}

double FullGaussianProcess::var(const double x[])
{
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    int n = sampleset->size();
    Eigen::VectorXd v = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(k_star);
    return cf->get(x_star, x_star) - v.dot(v);
}

void FullGaussianProcess::add_pattern(const double x[], double y)
{
	GaussianProcess::add_pattern(x, y);
	int n = sampleset->size()-1;
	// create kernel matrix if sampleset is empty
	if (n == 0)
	{
		L(0,0) = sqrt(cf->get(sampleset->x(0), sampleset->x(0)));
		cf->loghyper_changed = false;
		// recompute kernel matrix if necessary
	}
	else
		if (cf->loghyper_changed)
		{
			compute();
		}
		else
		{
			Eigen::VectorXd k(n);
			for (int i = 0; i<n; ++i) {
				k(i) = cf->get(sampleset->x(i), sampleset->x(n));
			}
			double kappa = cf->get(sampleset->x(n), sampleset->x(n));
			// resize L if necessary
			if (n >= L.rows()) {
				L.conservativeResize(n + initial_L_size, n + initial_L_size);
			}
			L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(k);
			L.block(n,0,1,n) = k.transpose();
			L(n,n) = sqrt(std::max(0.0, kappa - k.dot(k)));
		}
	alpha_needs_update = true;
}

bool FullGaussianProcess::set_y(size_t i, double y)
{
	if(GaussianProcess::set_y(i, y))
	{
		alpha_needs_update = true;
		return true;
	}
	return false;
}

double FullGaussianProcess::log_likelihood()
{
    compute();
    update_alpha();
    int n = sampleset->size();
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    double det = 2 * L.diagonal().head(n).array().log().sum();
    return -0.5*y.dot(alpha) - 0.5*det - 0.5*n*log2pi;
}

Eigen::VectorXd FullGaussianProcess::log_likelihood_gradient()
{
	compute();
	update_alpha();

	std::cout << "computing gradient" << std::endl;

	size_t n = sampleset->size();
	Eigen::VectorXd grad = Eigen::VectorXd::Zero(cf->get_param_dim());
	Eigen::VectorXd g(grad.size());
	Eigen::MatrixXd W = Eigen::MatrixXd::Identity(n, n);

	// compute kernel matrix inverse
	L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(W);
	L.topLeftCorner(n, n).triangularView<Eigen::Lower>().transpose().solveInPlace(W);

	std::cout << "L" << std::endl;

	W = alpha * alpha.transpose() - W;

	std::cout << "W" << std::endl;

	for(size_t i = 0; i < n; ++i) {
	  for(size_t j = 0; j <= i; ++j) {
		cf->grad(sampleset->x(i), sampleset->x(j), g);
		if (i==j) grad += W(i,j) * g * 0.5;
		else      grad += W(i,j) * g;
	  }
	}

	return grad;
}

void FullGaussianProcess::update_k_star(const Eigen::VectorXd& x_star)
{
	k_star.resize(sampleset->size());
	for(size_t i = 0; i < sampleset->size(); ++i) {
	  k_star(i) = cf->get(x_star, sampleset->x(i));
	}
}

void FullGaussianProcess::update_alpha()
{
	// can previously computed values be used?
	if (!alpha_needs_update) return;
	alpha_needs_update = false;
	alpha.resize(sampleset->size());
	// Map target values to VectorXd
	const std::vector<double>& targets = sampleset->y();
	Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
	int n = sampleset->size();
	alpha = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(y);
	L.topLeftCorner(n, n).triangularView<Eigen::Lower>().adjoint().solveInPlace(alpha);
}

void FullGaussianProcess::optimize(Optimizer* opt, size_t n, bool verbose)
{
	opt->maximize(static_cast<FullGaussianProcess*>(this), n, verbose);

//
//	for(size_t i = 0; i < sampleset->size(); ++i) {
//	  for(size_t j = 0; j < sampleset->size(); ++j) {
//		L(i, j) = cf->get(sampleset->x(i), sampleset->x(j));
//		if(i==j)
//			L(i,j) = L(i,j) + 1;
//	  }
//	}
//
//	std::ofstream ofsk("k.txt", std::ofstream::trunc);
//
//	for(size_t i = 0; i < sampleset->size(); ++i) {
//	  for(size_t j = 0; j < sampleset->size(); ++j) {
//		ofsk << L(i,j) << " ";
//	  }
//	  ofsk << std::endl;
//	}
//
//	ofsk.close();
//
//
//	// perform cholesky factorization
//	//solver.compute(K.selfadjointView<Eigen::Lower>());
//	L.topLeftCorner(n, n) = L.topLeftCorner(n, n).selfadjointView<Eigen::Lower>().llt().matrixL();
//
//	std::ofstream ofsl("l.txt", std::ofstream::trunc);
//
//	for(size_t i = 0; i < sampleset->size(); ++i) {
//	  for(size_t j = 0; j < sampleset->size(); ++j) {
//		ofsl << L(i,j) << " ";
//	  }
//	  ofsl << std::endl;
//	}
//
//	ofsl.close();
//
//	update_alpha();
//
//	std::ofstream ofsa("alpha.txt", std::ofstream::trunc);
//
//	ofsa << alpha;
//	ofsa.close();
}

void FullGaussianProcess::compute()
{
	// can previously computed values be used?
	if (!cf->loghyper_changed) return;
	cf->loghyper_changed = false;
	int n = sampleset->size();
	// resize L if necessary
	if (n > L.rows()) L.resize(n + initial_L_size, n + initial_L_size);
	// compute kernel matrix (lower triangle)
	for(size_t i = 0; i < sampleset->size(); ++i) {
	  for(size_t j = 0; j <= i; ++j) {
		L(i, j) = cf->get(sampleset->x(i), sampleset->x(j));
	  }
	}
	// perform cholesky factorization
	//solver.compute(K.selfadjointView<Eigen::Lower>());
	L.topLeftCorner(n, n) = L.topLeftCorner(n, n).selfadjointView<Eigen::Lower>().llt().matrixL();
	alpha_needs_update = true;
}

} /* namespace libgp */
